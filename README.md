# concept

Just some notes about what we want to do.

## Roadmap

Literally no idea how long this is gonna take. Gonna put some free time into it and perhaps enlist some others.

## Contributing

If you want to contribute, find a project that most closely represents your personal desires in life. Then, review the source
and find anything you don't like. Open a PR, and maybe we'll accept it. :)

## License

Nothing here yet... probably WTFPL in the future.
