# scoring

There are two general goals we'd like to accomplish: fair scoring and discouraging sandbagging. To this end, we use
non-traditional weighted scoring, a low-knowledge scoreboard system, and a dynamic end-of-competition.

Rather than doing the traditional scoring, jeopardy-style with a fixed or dynamic amount per challenge, let's try
percent completion.

## pure dynamic scoring: weighted percentage

We likely won't have very many challenges. On the other hand, we still want to accurately represent the difficulty
of each challenge. To accomplish this, each challenge will be weighted according to the number of solves that challenge
has compared to the number of total solves in the competition.

### weighting

Let's say there are five challenges, with `0`, `1`, `2`, `3`, and `4` solves. That means that the total number of solves
so far in the competition is `0+1+2+3+4=10`.

To weight appropriately, we'll use (competition solves / challenge solves) to weight each challenge. Thus, the challenges
are weighted:

1. No weight yet (divide by zero)
2. `10/1`
3. `10/2`
4. `10/3`
5. `10/4`

The total of these weights is `20.833...`

Now we can appropriately score each player.

### scoring

Now, based on our previous example, we simply score users by the sum of the weights of their chals.

Suppose you've solved challenges 2 and 4. Your score is `(10/1 + 10/3)/20.833... = 64%`. Not bad!

Suppose you've solved all the challenges so far. Your score is `100%`, but that doesn't mean you've done all the
challenges.

### caveats

At the end of the competition, there will necessarily be one team who has successfully solved a challenge that no
one else has (see the last section of this doc). This may over-weight that particular challenge.

## scoreboard

To discourage sandbagging, we introduce two constraints on scoreboard access:

1. You can always see your own score, but not your standings.
2. You can always see the number of solves on each challenge, but not who solved it.
3. You can see the full standings only upon submitting a new flag or when the competition is over.

This means that, at any given time, all you know about the standings are what they were when you last submitted a flag.
This prevents teams from resting on their laurels and also prevents sandbagging.

### caveats

Users might make a new account and submit a low-effort flag to get an update copy of the scores. To mitigate this, we'll
only send the top 5 teams + the 2 teams in front of and behind the user.

## competition end

The competition ends under two conditions:

1. The time limit expires.
2. One team successfully completes all challenges.

This also discourages sandbagging and resting on laurels as there is a *strong* incentive to complete all challenges as
quickly as possible.
