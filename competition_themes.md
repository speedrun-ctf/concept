# themes

Each competition will have a different theme. Here are the ideas we have so far:

1. autorev/autopwn, where the user is provided access to a server that generates a new target per attempt
2. kernel, where each challenge is a kernel pwn
3. hypervisor/emulator, where each challenge is a hypervisor/emulator escape
4. jit, where each challenge is a jit exploit

And, of course, nothing stops us from having a little bit of unthemed competitions here and there.
