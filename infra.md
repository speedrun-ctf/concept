# infra

We've been talking a little bit about an infra approach that may be better suited towards our particular challenge
style. This infrastructure would allow for instancing between challenges and user interfaces to the CTF management
system to minimise player interference. Additionally, only one port would need to be opened (443).

We would perform this by inspecting SNI of the request to determine the intended service, then inspecting the
certificate to determine the user. Containers would be spawned per challenge, then their TCP endpoints proxied.

## instancing

The user will access the CTF environment using SNI and a client certificate. Instances of challenges and the CTF's
TUI will be spawned for each unique SNI/client pair upon connect and destroyed upon disconnect. Multiple simultaneous
connections will connect to the same instance, and only once all active sessions are disconnected will the instance be
destroyed.

TLS connections will be proxied to the relevant instance generated. This mitigates user-to-user interference.

## CTF management system

Just like challenges, the actual management system the user connects to will be instanced.

Before the competition starts, a CA will be generated. A central server to only be accessed on sign up will be
started with access to the generated CA.

### sign up

When a new user connects to the service, they connect over SSL with an SNI of 'ctf'. They won't have a certificate
with their username yet, which will indicate that this is a new user. They will default connect to the central
server.

They'll be given the opportunity to pick a username, which will be checked for duplicates. The server will respond
with the openssl commands to generate an appropriate key + csr for that particular name. The user will then
paste in their csr, which will be signed by CA after inserting the username into the database (and checked for
duplicates one last time). If signing fails, the username will be removed. The fullchain cert will be returned to
the user.

The user is now registered and will use the key + fullchain cert provided to connect in the future.

In addition, a large secret will be generated for the serverside interactions. The user will never need to see this
secret.

### connecting in the future

The user now connects with an SNI of 'ctf' and their issued key/cert. Upon connection, the CTF TUI is instanced
like any other challenge and has the user's secret provided as an argument. The user will be able to use this
TUI to access challenge descriptions, their current score, and submit flags.

The TUI instance will make requests to the central server in order to access challenge descriptions, score, and flag
submission. The central server will authenticate any request based on the issued secret, so even if the TUI instance
is compromised, no other users are compromised.

#### implementation specifics

All interactions should only be made available after the competition start time, both in TUI and in the central server.

Submission of a flag will respond with the user's current standing as described in [scoring](scoring.md).

## challenges

Each challenge description will specify an SNI to use and provide a solver template to connect to the challenge.
